package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;
import ru.tsc.karbainova.tm.endpoint.User;

public class AuthLoginCommand extends AbstractCommand {
    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login app";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        Session session = serviceLocator.getSessionEndpoint().openSession(login, password);
        serviceLocator.setSession(session);
//        serviceLocator.getAuthEndpoint().loginAuth(session, login, password);

        System.out.println("[OK]");
    }
}
