package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpointTest {
    @NonNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NonNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NonNull
    private static final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NonNull
    private static final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static Session session;
    @Nullable Project project;
    private static String userLogin = "test";

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession(userLogin, userLogin);
    }

    @Before
    public void before() {
        project = new Project();
        project.setName("Project");
        project.setId("1");
        projectEndpoint.addProject(project);
    }

    @After
    public void after() {
        projectEndpoint.removeByIdPro(session, "1");
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NonNull final Project projectById = projectEndpoint.findByNameProject(session, "Project");
        Assert.assertNotNull(projectById);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NonNull final List<Project> projects = projectEndpoint.findAllProject(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByUserId() {
        @NonNull final List<Project> projects = projectEndpoint.findAllProject(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByName() {
        @NonNull final Project projects = projectEndpoint.findByNameProject(session, project.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByErrorName() {
        @NonNull final Project projects = projectEndpoint.findByNameProject(session, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        projectEndpoint.removeByIdPro(session, project.getId());
        Assert.assertNull(projectEndpoint.findByNameProject(session, "Project"));
    }

}
