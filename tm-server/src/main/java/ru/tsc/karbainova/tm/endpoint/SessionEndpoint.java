package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint {
    private ServiceLocator serviceLocator;
    private ISessionService sessionService;

    public SessionEndpoint(
            @NonNull final ServiceLocator serviceLocator,
            @NonNull final ISessionService sessionService
    ) {
        this.serviceLocator = serviceLocator;
        this.sessionService = sessionService;
    }

//    @WebMethod
//    public boolean checkDataAccessSession(String login, String password) {
//        return sessionService.checkDataAccess(login, password);
//    }

    @WebMethod
    public Session openSession(
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        sessionService.close(session);
    }
}
