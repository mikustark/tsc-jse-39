package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void create(AbstractCommand command);

}
