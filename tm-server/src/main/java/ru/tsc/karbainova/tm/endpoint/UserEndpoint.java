package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.Session;
import ru.tsc.karbainova.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint {
    private IUserService userService;
    private ServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(
            final ServiceLocator serviceLocator,
            final IUserService userService
    ) {
        this.serviceLocator = serviceLocator;
        this.userService = userService;
    }

    @WebMethod
    public User createUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password) {
        serviceLocator.getSessionService().validate(session);
        return userService.create(login, password);
    }

    @WebMethod
    public User setPasswordUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "password") String password) {
        serviceLocator.getSessionService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public User createUserWithEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email) {
        serviceLocator.getSessionService().validate(session);
        return userService.create(login, password, email);
    }

    @WebMethod
    public User updateUserUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "firstName") String firstName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "middleName") String middleName) {
        serviceLocator.getSessionService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }
}
