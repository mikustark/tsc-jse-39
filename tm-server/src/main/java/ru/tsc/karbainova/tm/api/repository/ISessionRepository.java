package ru.tsc.karbainova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.tsc.karbainova.tm.dto.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session" +
            "(id, user_id, signature, timestamp)" +
            "VALUES(#{id}, #{user_id}, #{signature}, #{timestamp})")
    void add(
            @Param("id") String id,
            @Param("user_id") String user_id,
            @Param("signature") String signature,
            @Param("timestamp") Long timestamp
    );

    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "id", property = "id")
    })
    List<Session> findAll();

    @Select("SELECT * FROM tm_session WHERE id = #{id}")
    List<Session> exists(final String id);

    @Delete("SELECT * FROM tm_session WHERE id=#{id}")
    void remove(final String id);

}
