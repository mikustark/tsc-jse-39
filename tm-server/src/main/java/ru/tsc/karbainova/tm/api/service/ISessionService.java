package ru.tsc.karbainova.tm.api.service;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {
    boolean checkDataAccess(String login, String password);

    @SneakyThrows
    Session add(Session session);

    Session open(String login, String password);

    Session sign(Session session);

    @SneakyThrows
    List<Session> findAll();

    List<Session> getListSessionByUserId(String userId);

    void close(Session session);

    void closeAll(Session session);

    void validate(Session session);

    void validate(Session session, Role role);

    //    void signOutByLogin(String login);
    void signOutByUserId(String userId);

}
